# Practica de Big Data de Algebra

Esta es mi práctica de Algebra.
El notebook está en el enlace de abajo. En el propio fichero está explicado todo lo que voy haciendo y las respuestas a las preguntas.
Espero haberme explicado bien. Cualquier duda, por favor, me lo comentas.

## Enlace al notebook de Jupyter ALG00_Practica_JuanManuel_Iriondo.ipynb

https://drive.google.com/file/d/1L9mzM1_Qz1n2XFOg9c50vhnoP47Ty2sj/view?usp=sharing

